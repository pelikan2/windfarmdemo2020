package sk.fri.uniza.resources;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;
import retrofit2.http.Body;
import sk.fri.uniza.api.Device;
import sk.fri.uniza.db.DevicesDao;
import sk.fri.uniza.db.LiteWeatherObjDao;
import sk.fri.uniza.openweathermap.LiteWeatherOBJ;
import sk.fri.uniza.openweathermap.OpenWeatherOBJ;


import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/authDevice")
@Produces(MediaType.APPLICATION_JSON)
public class Dev_Resource {
    private DevicesDao dao_device;
    LiteWeatherObjDao weather_Dao;

    public Dev_Resource(DevicesDao dao_device, LiteWeatherObjDao weather_Dao)  {
        this.dao_device = dao_device;
        this.weather_Dao = weather_Dao;
    }

    @PermitAll
    @GET
    @UnitOfWork
    public Response getEmployees(@Auth Device devices) {
        return Response.ok().build();
    }

    @RolesAllowed( "default" )
    @GET
    @Path("/{id}")
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDevice_WithID(@PathParam("id") Integer id, @Auth Device devices) {
        Device device = dao_device.getDeviceWithId_DB(id);
        if (device != null)
            return Response.ok(device).build();
        else
            return Response.status(Response.Status.NOT_FOUND).build();
    }

    @DELETE
    @UnitOfWork
    public Response deviceDelete(@QueryParam("id") Integer id) {
        dao_device.delete(dao_device.getDeviceWithId_DB(id));
        return Response.temporaryRedirect(URI.create("http://localhost:8080/personsP")).build();
    }

    @GET
    @Path("/addDev")
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Device write(@QueryParam("name") String _name,
                        @QueryParam("location") String locate,
                        @QueryParam("pass") String password) {
        Device aa = new Device(_name,locate,password,"url");
        dao_device.save(aa);
        return aa;
    }

    @GET
    @Path("/getWeather")
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    public List<LiteWeatherOBJ> select(@QueryParam("m") String miesto) {
        List<LiteWeatherOBJ> aa = weather_Dao.findDeviceWithName_DB(miesto);
        return aa;
    }

    @GET
    @Path("/selectDev")
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Device> select() {
        List<Device> aa = dao_device.getAll();
        return aa;
    }




    @POST
    @Path("weather")
    @RolesAllowed( "default" )
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    public OpenWeatherOBJ newWeather(@Body OpenWeatherOBJ obj)  {
        LiteWeatherOBJ newData = new LiteWeatherOBJ(obj);
        weather_Dao.save(newData);
        System.out.println("newData"+newData.getName()+" temp:"+newData.getTemp());
        return obj;
    }

}
